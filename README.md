# dummy-app

## Creating and running a Docker image, locally.
Before using CI/CD pipelines, I wanted to build the project image locally and see that if it was working.  

![mdres](./mdres/pic1.png)

## Creating a simple pipline.

```yml
stages:
  - fail-first # Test easy to fail things here. (Saves time later.)
  - docker-build # Build the docker image.
  - test # Run tests on the app. (Unit, linting etc.)
  - integration-tests # Run integration tests with the image.
  - deploy # Push the image to a registry and deploy it afterwards.
```

![pic4](./mdres/pic4.png)
![pic5](./mdres/pic5.png)

At this point checking the `.gitlab-ci.yml` wold be better. I have explained my tought process in the comments of that file.

You can also check the latest pipelines's logs.

**Note:** I did not push the image to ECR registry at the stage ``deploy`` as I said I did here. I did it as soon as building the image at the ``docker-build`` stage. 

I don't actually think pushing it to the registry before testing the image is a good idea. (I was just trying to finish the pushing the image to ECR registry part as soon as possible.)



---

# Deployment

This is the part I was a bit lost. And I couldn't complete the whole task. I'm new to AWS, Terraform, ECR etc. Here is what I did so far.

- I have created a registry and a cluster on ECS using Terraform. 
- Created my docker image through pipelines and pushed it to the ECS registry.

## AWS Account
I have created an AWS account and created a user with ``admin`` privilages. (Probably overkill, but its is a dummy project.)

I have also installed the `aws-cli` tool to my computer. Terraform needed that for registry authentication.

I registered the ``AWS Access Key ID`` and ``AWS Access Key`` to the awc-cli tool on my local machine. Also I did the same by registering them to GitLab's environment variables. aws-cli and terraform needed them.

### Registry Login
Default.
```
aws ecr get-login-password --region eu-west-3 | docker login --username AWS --password-stdin  190078620816.dkr.ecr.eu-west-3.amazonaws.com
```

Non tty sessions.
```
docker login -u AWS -p $(aws ecr get-login-password --region eu-west-3) 190078620816.dkr.ecr.eu-west-3.amazonaws.com
```

## Terraform

Terraform handles infrastructures in a programmatical way. It eliminites usage of GUI and helps automating trivial stuff. I'm not sure about all the capabilities of this thing.

I have used it to create a registry and a cluster only. They were one time operations so I did not include the process into the pipeline.

``terraform init``

main.tf
```
provider "aws" {
  region = "eu-west-3"
}

resource "aws_ecr_repository" "dummy-app" {
  name = "dummy-app" # repository
}

resource "aws_ecs_cluster" "dummy-app-cluster" {
  name = "dummy-app-cluster" # Naming the cluster
}
```

``terraform plan``

``terraform apply``

**Note:** I have seen gitlab having some sort of terraform integration but I don't know how to use it.

![pic3](./mdres/pic3.png)